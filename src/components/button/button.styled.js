import styled from "styled-components";

export const ButtonStyled = styled.button`
  overflow: hidden;
  background-color: transparent;
  outline: none;
  position: relative;
  cursor: pointer;
  top: 20px;
  left: calc((100vw - 731px) / 2 + 220px);
  color: #fff;
  font-size: 20px;
  border: none;
  box-shadow: rgba(130, 170, 183, 0.6) 0px 2px 6px;
  text-align: center;
  width: 250px;
  height: 60px;
  box-sizing: border-box;
`;
